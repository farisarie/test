//
//  BaseViewController.swift
//  TimePad
//
//  Created by yoga arie on 18/05/22.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.prefersLargeTitles = true
        setupColor()
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
            if self.traitCollection.hasDifferentColorAppearance(comparedTo: previousTraitCollection){
                setupColor()
            }
    }
    
    
    func setupColor(){
            view.backgroundColor = self.traitCollection.userInterfaceStyle == .dark ? UIColor.backgroundDark : UIColor.backgroundLight
    }

}
