//
//  AddTaskViewController.swift
//  Timepad
//
//  Created by yoga arie on 23/11/22.
//

import UIKit

class AddTaskViewController: BaseViewController {
    weak var titleLabel: UILabel!
    weak var titleTextField: UITextField!
    weak var categoryTitleLabel: UILabel!
    weak var categoryStackView: UIStackView!
    weak var tagTitleLabel: UILabel!
    weak var tagStackView: UIStackView!
    weak var saveButton: UIButton!
    var selectedCategory: Category?
    var selectedTag: Tag?
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Add New Task"
        setup()
    }
    
    func setup() {
        let titleLabel = UILabel(frame: .zero)
        view.addSubview(titleLabel)
        self.titleLabel = titleLabel
        titleLabel.text = "Task Title"
        titleLabel.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            titleLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
            titleLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),
            titleLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 16)
        ])
        
        let titleTextField = UITextField(frame: .zero)
        view.addSubview(titleTextField)
        self.titleTextField = titleTextField
        titleTextField.placeholder = "Title"
        titleTextField.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        titleTextField.borderStyle = .none
        titleTextField.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            titleTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
            titleTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),
            titleTextField.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 4),
            titleTextField.heightAnchor.constraint(equalToConstant: 36)
        ])
        
        let line = UIView(frame: .zero)
        view.addSubview(line)
        line.backgroundColor = UIColor.separator
        line.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            line.heightAnchor.constraint(equalToConstant: 1),
            line.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
            line.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),
            line.topAnchor.constraint(equalTo: titleTextField.bottomAnchor)
        ])
        
        let categoryTitleLabel = UILabel(frame: .zero)
        view.addSubview(categoryTitleLabel)
        self.categoryTitleLabel = categoryTitleLabel
        categoryTitleLabel.text = "Task Title"
        categoryTitleLabel.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        categoryTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            categoryTitleLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
            categoryTitleLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),
            categoryTitleLabel.topAnchor.constraint(equalTo: line.bottomAnchor, constant: 16)
        ])
        
        
        let categoryStackView = UIStackView()
        view.addSubview(categoryStackView)
        self.categoryStackView = categoryStackView
        categoryStackView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            categoryStackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
            categoryStackView.trailingAnchor.constraint(lessThanOrEqualTo: view.trailingAnchor, constant: -16),
            categoryStackView.topAnchor.constraint(equalTo: categoryTitleLabel.bottomAnchor, constant: 8)
        ])
        
        categoryStackView.axis = .horizontal
        categoryStackView.distribution = .fill
        categoryStackView.alignment = .fill
        categoryStackView.spacing = 12
        
        let categories = Category.allCases
        for i in 0..<categories.count{
            let category = categories[i]
            let button = UIButton(type: .system)
            categoryStackView.addArrangedSubview(button)
            button.setImage(category.icon.withRenderingMode(.alwaysOriginal), for: .normal)
            button.setTitle(nil, for: .normal)
            button.layer.cornerRadius = 22
            button.layer.masksToBounds = true
            button.tag = i
            button.addTarget(self, action: #selector(self.categoryButtonTapped(_:)), for: .touchUpInside)
            button.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                button.heightAnchor.constraint(equalToConstant: 44),
                button.widthAnchor.constraint(equalToConstant: 44)
            ])
        }
        
        let tagTitleLabel = UILabel(frame: .zero)
        view.addSubview(tagTitleLabel)
        self.tagTitleLabel = tagTitleLabel
        tagTitleLabel.text = "Tag"
        tagTitleLabel.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        tagTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            tagTitleLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
            tagTitleLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),
            tagTitleLabel.topAnchor.constraint(equalTo: categoryStackView.bottomAnchor, constant: 16)
        ])
        
        let tagStackView = UIStackView()
        view.addSubview(tagStackView)
        self.tagStackView = tagStackView
        tagStackView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            tagStackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
            tagStackView.trailingAnchor.constraint(lessThanOrEqualTo: view.trailingAnchor, constant: -16),
            tagStackView.topAnchor.constraint(equalTo: tagTitleLabel.bottomAnchor, constant: 8)
        ])
        
        tagStackView.axis = .horizontal
        tagStackView.distribution = .fill
        tagStackView.alignment = .fill
        tagStackView.spacing = 12
        
        let tags = Tag.allCases
        for i in 0..<tags.count{
            let tag = tags[i]
            let button = UIButton(type: .system)
            tagStackView.addArrangedSubview(button)
            button.titleLabel?.font = UIFont.systemFont(ofSize: 12, weight: .regular)
            button.contentEdgeInsets = UIEdgeInsets(top: 4, left: 8, bottom: 4, right: 8)
            button.layer.cornerRadius = 6
            button.layer.masksToBounds = true
            button.tintColor = tag.titleColor
            button.backgroundColor = traitCollection.userInterfaceStyle == .dark ? tag.backgroundDarkColor : tag.backgroundColor
       
            button.setTitle(tag.name, for: .normal)
            button.tag = i
            button.addTarget(self, action: #selector(self.tagButtonTapped(_:)), for: .touchUpInside)
            
            let saveButton = UIButton(type: .system)
            view.addSubview(saveButton)
            self.saveButton = saveButton
            saveButton.setTitle("Save", for: .normal)
            let isDark = traitCollection.userInterfaceStyle == .dark
            saveButton.setTitleColor(isDark ? UIColor.white : UIColor(rgb: 0x070417), for: .normal)
            saveButton.backgroundColor = isDark ? UIColor(rgb: 0x1B143F) : UIColor(rgb: 0xE9E9FF)
            saveButton.titleLabel?.font = UIFont.systemFont(ofSize: 18, weight: .medium)
            saveButton.layer.cornerRadius = 8
            saveButton.layer.masksToBounds = true
            saveButton.addTarget(self, action: #selector(self.saveButtonTapped(_:)), for: .touchUpInside)
            saveButton.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                saveButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 40),
                saveButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -40),
                saveButton.heightAnchor.constraint(equalToConstant: 60),
                saveButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -40)
            ])
        }
        
    }
    @objc func categoryButtonTapped(_ sender: UIButton){
        let categories = Category.allCases
        let category = categories[sender.tag]
        
        selectedCategory = category
        categoryStackView.arrangedSubviews.forEach { view in
            let button = view as! UIButton
            button.isEnabled = button != sender
            if button.isEnabled{
                button.layer.borderColor = UIColor.clear.cgColor
                button.layer.borderWidth = 0
            } else {
                let isDark = traitCollection.userInterfaceStyle == .dark
               
                button.layer.borderColor = (isDark ? UIColor.white : UIColor.darkGray).cgColor
                button.layer.borderWidth = 2
            }
        }    }
    
    @objc func tagButtonTapped(_ sender: UIButton){
        let tags = Tag.allCases
        let tag = tags[sender.tag]
       
        selectedTag = tag
        tagStackView.arrangedSubviews.forEach { view in
            let button = view as! UIButton
            button.isEnabled = button != sender
            if button.isEnabled{
                button.layer.borderColor = UIColor.clear.cgColor
                button.layer.borderWidth = 0
            } else {
                button.layer.borderColor = tag.titleColor.cgColor
                button.layer.borderWidth = 2
            }
        }    }
    
    @objc func saveButtonTapped(_ sender: UIButton){
        
    }

}

// MARK: - UIViewController
extension UIViewController {
    func presentAddTaskViewController() {
        let viewController = AddTaskViewController()
        let navigationController = UINavigationController(rootViewController: viewController)
        present(navigationController, animated: true)
    }
}
